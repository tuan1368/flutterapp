import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() => runApp(new MyApp()); // Function one line

// StatefulWidget can change object properties

class RandomEnglishWords extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RandomEnglishWordsState();
  }
}
class RandomEnglishWordsState extends State<RandomEnglishWords> {
  final _word = <WordPair>[];
  final _checkedWords = new Set<WordPair>(); // Set contains "no duplicate items"
  @override
  Widget build(BuildContext context) {
    final wordPair = new WordPair.random();
// Now we replace this with a Scaffold widget which contains Listview
    return new Scaffold(
        appBar: new AppBar(
          title: new Center(child: new Text("List of English words")),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.list), onPressed: _pushToSaveWords)
          ],
        ),
        body: new ListView.builder(itemBuilder: (context, index) {
          if (index >= _word.length) {
            _word.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_word[index], index);
        }));
  }

  Widget _buildRow(WordPair wordPair, int index) {
    final textColor = index % 2 == 0 ? Colors.red : Colors.blue;
    final isChecked = _checkedWords.contains(wordPair);
    return new ListTile(
      leading: new Icon(
        isChecked ? Icons.check_box : Icons.check_box_outline_blank,
        color: textColor,
      ),
      title: new Text(
        wordPair.asPascalCase,
        style: new TextStyle(fontSize: 28.0, color: textColor),
      ),
      onTap: () {
        setState(() {
          // This is an anonymous function
          if (isChecked) {
            _checkedWords.remove(wordPair); //Remove item in a Set
          } else {
            _checkedWords.add(wordPair); //Add item to a Set
          }
        });
      },
    );
  }

  _pushToSaveWords() {
//  print("You pressed to the right icon");
// To navigate, you must have a "route"
    final pageRoute = new MaterialPageRoute(builder: (context) {
      // map function = Convert this list to another list (maybe different object's type)
      final listTitle = _checkedWords.map((wordPair) {
        return new ListTile(
          title: new Text(wordPair.asPascalCase,
          style: new TextStyle(fontSize: 28.0,
          fontWeight: FontWeight.bold),),
        );
      });
      // Now return a Widget we choose Scafold
      return new Scaffold(
        appBar: new AppBar(
          title: new Center(child: new Text("Checked words"),
          ),
        ),
        body: new ListView(children: listTitle.toList(),), // Lazy list(iterable) =>List
      );
    });
    Navigator.of(context).push(pageRoute);
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final wordPair = new WordPair.random();

    return new MaterialApp(
        title: "This is my first Flutter app", home: new RandomEnglishWords());
  }
}
